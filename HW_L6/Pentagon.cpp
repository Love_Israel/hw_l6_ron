#include "Pentagon.h"

Pentagon ::Pentagon (std::string nam , std::string col , double side) : Shape (nam , col)
{
	_name = nam;
	_color = col;
	_side = side;
}

Pentagon :: ~Pentagon()
{
	//nothing
}

void Pentagon :: setSide(double side) 
{
	_side = side;
}
double Pentagon :: getSide() 
{
	return (_side);
}

double Pentagon :: CalArea() 
{
	MathUtils math = MathUtils();
	return (math.calPentagonArea(_side));
}
double Pentagon :: CalCircumference() 
{
	return (_side * 5);
}
void Pentagon :: draw() 
{
	std::cout << "name is " << getName() << " color is " << getColor() << " area is " << this->CalArea() << " circumference is " << this->CalCircumference();
}