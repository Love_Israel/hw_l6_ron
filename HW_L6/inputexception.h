#pragma once
#include <iostream>

class InputException : public std::exception 
{
public:
	const char* what () const 
	{
		return ("invalid input\n");
	}
};
