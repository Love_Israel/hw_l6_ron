#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "inputexception.h"
#include "Hexagon.h"
#include "Pentagon.h"

void checkForException(bool isFail);

int main()
{
	std::string nam, col; 
	double rad = 0, ang = 0, ang2 = 180; 
	int height = 0, width = 0;
	Circle circ = Circle(col, nam, rad);
	quadrilateral quad = quadrilateral(nam, col, width, height);
	rectangle rec = rectangle(nam, col, width, height);
	parallelogram para = parallelogram(nam, col, width, height, ang, ang2);
	Pentagon pen = Pentagon(nam, col, 0);
	Hexagon hex = Hexagon(nam, col, 0);
	std::string shapeInput;

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape* ptrpen = &pen;
	Shape* ptrhex = &hex;


	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; 
	char shapetype;
	char x = 'y';
	while (x != 'x') {
		start:
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram , s = Pentagon , h = Hexagon" << std::endl;
		std::cin >> shapeInput;
		if (shapeInput.size() != 1) { //if input is more then one letter
			std::cout << "wrong input";
			goto start;
		}
		shapetype = shapeInput[0];
try
		{

			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				checkForException(std::cin.fail());
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				checkForException(std::cin.fail());
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				checkForException(std::cin.fail());
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				checkForException(std::cin.fail());
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
				break;
			case 's':
				std::cout << "enter name, color, side" << std::endl;
				std::cin >> nam >> col >> height;
				pen.setName(nam);
				pen.setColor(col);
				pen.setSide(height);
				ptrpen->draw();
				break;
			case 'h':
				std::cout << "enter name, color, side" << std::endl;
				std::cin >> nam >> col >> height;
				hex.setName(nam);
				hex.setColor(col);
				hex.setSide(height);
				ptrhex->draw();
				break;

			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin >> x;
		}
		catch (shapeException e)
		{			
			printf(e.what());
		}
		catch (InputException inex) //if input exception
		{
			printf(inex.what());
			std::cin.clear();
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}

		system("pause");
		return 0;
}

void checkForException(bool isFail) //check if there is exception 
{
	if (isFail) 
	{
		throw (InputException());
	}
}